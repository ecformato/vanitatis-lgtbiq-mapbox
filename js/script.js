/*
* Comenzar el proceso de análisis y visualización cuando el documento esté listo
*/
if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
    initData();
} else {
    document.addEventListener("DOMContentLoaded", initData);
}

function initData() {
    createTimeslider();
    createMap();
}

/* Variables para timeslider */
let currentValue = 2022;

const firstValue = 2000;
const lastValue = 2022;
const yearsDifference = (lastValue - firstValue) + 1;
let legalColor = '#ecb901', ilegalColor = '#f1eeea';

/* Elementos del DOM que afectan o se ven afectados por el timeslider */
let sliderRange = document.getElementById('slider');
let sliderDate = document.getElementById('sliderDate');
let playButton = document.getElementById('btnPlay');
let pauseButton = document.getElementById('btnPause');
let sliderInterval;

/* 
*
* Funciones para configurar el timeslider (en consonancia con los datos del archivo) 
*
*/
function createTimeslider(){
    let size = yearsDifference, step = 1;

    sliderRange.size = size;
    sliderRange.min = firstValue;
    sliderRange.max = lastValue;
    sliderRange.step = step;
    sliderRange.value = lastValue;
    
    /* Los siguientes eventos tienen la capacidad de modificar lo que se muestra en el mapa */
    playButton.onclick = function () {
        sliderInterval = setInterval(setNewValue,300);
        playButton.style.display = 'none';
        pauseButton.style.display = 'inline-block';    
    }

    pauseButton.onclick = function () {
        clearInterval(sliderInterval);
        playButton.style.display = 'inline-block';
        pauseButton.style.display = 'none';
    }

    sliderRange.oninput = function () {
        let currentValue = parseInt(sliderRange.value);
        showSliderDate(currentValue);
        filterBy(map, currentValue);
    }
}

/* Da nuevos valores al slider */
function setNewValue() {
    let value = parseInt(sliderRange.value);

    if(value == lastValue) {
        sliderRange.value = firstValue;
    } else {
        sliderRange.value = value + 1;
    }

    currentValue = sliderRange.value;
    
    showSliderDate(currentValue);
    filterBy(map, currentValue);

    if (currentValue == 2022) {
        clearInterval(sliderInterval);
        playButton.style.display = 'inline-block';
        pauseButton.style.display = 'none';
    }
}

function showSliderDate(currentValue){
    document.getElementById('sliderDate').textContent = currentValue;
}

///// Mapa
let map;

/*
* Función donde almacenamos todo lo relativo al mapa
*/
function createMap(){    
    mapboxgl.accessToken = 'pk.eyJ1IjoiZGF0b3MtZWxjb25maWRlbmNpYWwiLCJhIjoiY2syNHNsMGN0MWRyZDNubnkwaHFiZGw4ayJ9.eKIhDFvX3YotsAEtGihnAw';

    /* Variable para escala*/
    let scale = new mapboxgl.ScaleControl({
        maxWidth: 100,
        unit: 'metric'
    });

    /* Variable para poder navegar sobre el mapa */
    let nav = new mapboxgl.NavigationControl();

    /* Inicializar variable para trabajar con popups */
    let popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
    });

    /* Ajustar ZOOM, MINZOOM Y CENTER EN FUNCIÓN DEL DISPOSITIVO */
    let windowWidth = window.innerWidth; 
    zoom = windowWidth < 600 ? 0 : 0.5;
    minZoom = windowWidth < 600 ? 0 : 0.5;
    center = [10, 31.899]; //mapWidth > 600 ? [7.371, 31.899] : [13.371, 31.899];

    /* Inicio de la configuración del mapa */
    map = new mapboxgl.Map({
        attributionControl: false,
        container: 'map',
        zoom: zoom,
        minZoom: minZoom,
        center: center,
        style: 'mapbox://styles/datos-elconfidencial/ckqgb4wbngady17p8rwhu4tzc'
    });

    map.addControl(scale);
    map.addControl(nav);

    map.on('load', () => {

        map.addSource('paises', {
            'type': 'vector',
            'url': 'mapbox://datos-elconfidencial.cubajhrw',
            promoteId: 'admin'
        });

        marriageData.map((item) => {
            map.setFeatureState({
                source: 'paises',
                sourceLayer: 'all-countries-5zmmjl',
                id: item['Country']
            }, {
                //Nombre (en inglés)
                name_en: item['Country'],
                name_es: item['País'],
                marriageYear: +item['Año'],
                notas: item['Notas']
            });
        });

        map.addLayer({
            'id': 'paises_fill',
            'source': 'paises',
            'source-layer': 'all-countries-5zmmjl',
            'type': 'fill',
            'paint': {
                'fill-color': [
                    'case',
                    ['!=', ['feature-state', 'marriageYear'], null],
                    legalColor,
                    ilegalColor
                ],
                'fill-opacity': 0.9
            }      
        }, 'admin-0-boundary');

        map.scrollZoom.disable();
        //map.dragPan.disable();

        filterBy(map, currentValue);
        bind_event(popup, 'paises_fill');
    });
}

/* Función para el filtrado de datos */
function filterBy(map, currentValue) {
    map.setPaintProperty('paises_fill', 'fill-color', 
    [
        'case',
        ['!=', ['feature-state', 'marriageYear'], null],
        [
            'case',
            ['<=', ['feature-state', 'marriageYear'], +currentValue],
            legalColor,
            ilegalColor
        ],
        ilegalColor
    ]);
}

//////
// Helpers
//////

/* En el layout específico (con id) cuando el ratón se mueva por el mapa en un elemento en concreto, que nos muestre un popup */
function bind_event(popup, id){
    map.on('mouseover', id, function(e){
        let props = e.features[0].properties;
        let state = e.features[0].state;
        
        map.getCanvas().style.cursor = 'pointer';
        var coordinates = e.lngLat;        
        var tooltipText = get_tooltip_text(id, props, state);

        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        popup.setLngLat(coordinates)
            .setHTML(tooltipText)
            .addTo(map); 
    });
    map.on('mouseleave', id, function() {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });
}

/* Texto del tooltip */
function get_tooltip_text(id, props, state){
    let html = `<div class="popup-content">`;

    if(state.name_es) {
        let pais = `<p class="popup-title">${state.name_es}</p>`;
        let texto = `<p class="popup-text">Año de legaliz.: ${state.marriageYear}</p>`;
        let notas = state.notas ? `<p class="popup-text">${state.notas}</p>` : '';
        html += `${pais}${texto}${notas}`;
    } else {
        let pais = `<p class="popup-title">${props['Nombre del país']}</p>`;
        let texto = `<p class="popup-text">Este país no ha legalizado el matrimonio homosexual</p>`;
        html += `${pais}${texto}`;
    }

    html += '</div>'

    return html;                
}

function getNumberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}